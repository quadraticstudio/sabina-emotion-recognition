import * as React from 'react';
import ParserController, { INeuralItem, NeuralEmotionTypeEnum, ALL_DATA_SET } from './lib/parser';
import { TensorFlowImplementation } from './lib/tensorflow';

interface IAppState {
  log: string[];
}

let MAX_PHOTO = 1000;

export default class App extends React.Component<any, IAppState> {
  parserController: ParserController = null;
  neural: TensorFlowImplementation = null;

  constructor(props) {
    super(props);
    this.state = {
      log: []
    };
    this.parserController = new ParserController();
    this.neural = new TensorFlowImplementation(this.parserController, this.log);
  }

  log = (...msg) => {
    return new Promise<void>(resolve => {
      let { log } = this.state;
      console.log(...msg);
      resolve();
      log.push(msg.map(m => (typeof m == 'object' ? JSON.stringify(m) : `${m}`)).join(' '));
      this.setState({ log }, () => setTimeout(() => resolve(), 100));
    });
  };

  onLoadAll = async () => {
    await this.log(`load from 0 to ${ALL_DATA_SET} datasets`);
    for (let i = 0; i < ALL_DATA_SET; i++) {
      let partitial = this.parserController.getPartitial(i);
      if (!partitial) continue;
      await partitial.load();
      await this.log(partitial.url, 'successfully loaded');
    }
  };

  onStart = async () => {
    let neural = this.neural;
    await neural.start();
    return neural;
  };

  onShowImage = async (pixels: number[]) => {
    let canvas = document.getElementById('canvas') as HTMLCanvasElement;
    var ctx = canvas.getContext('2d');
    canvas.width = 48;
    canvas.height = 48;
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var data = imgData.data;
    for (let i = 0, j = 0; i < data.length; i += 4, j++) {
      let pixel = pixels[j];
      data[i] = pixel;
      data[i + 1] = pixel;
      data[i + 2] = pixel;
      data[i + 3] = 255;
    }
    ctx.putImageData(imgData, 0, 0);
  };

  onTest = async () => {
    let indexPartitial = Math.floor(Math.random() * (ALL_DATA_SET - 1));
    let partitial = this.parserController.getPartitial(indexPartitial);
    let neural = await this.onStart();

    let itemsOriginal = await partitial.load();

    let index = Math.floor(Math.random() * itemsOriginal.length);
    let itemOriginal = itemsOriginal[index];
    let itemNormalized = partitial.itemsNormalized[index];

    let res = neural.predict(itemNormalized.pixels);
    await this.onShowImage(itemOriginal.pixels);
    console.log(res.map(m => m.value), NeuralEmotionTypeEnum[itemOriginal.emotionType]);
    // await this.log(res, NeuralEmotionTypeEnum[item.emotionType]);
  };

  getResult = (
    data: {
      key: string;
      value: number;
    }[],
    countOfResults: number = 1
  ) => {
    let keys = data.map(m => m.key);
    let numbers = data.map(m => m.value);
    let indexes: number[] = [];
    for (let i = 0; i < countOfResults; i++) {
      let maxNumber = Math.max(...numbers);
      let index = numbers.indexOf(maxNumber);
      numbers = numbers.filter(m => m != maxNumber);
      indexes.push(index);
    }
    return indexes.map(m => keys[m]);
  };

  render() {
    return (
      <div id="App" style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', flexDirection: 'row', margin: 15 }}>
          <canvas id="canvas" width="48" height="48" />
          <button onClick={() => this.onLoadAll()}>Load all</button>
          <button onClick={() => this.onStart()}>Start training</button>
          <button onClick={() => this.onTest()}>showImage</button>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', border: '1px solid black', width: '100%', minHeight: 50, padding: 10 }}>
          <h1>Log</h1>
          {this.state.log.map((m, i) => (
            <div key={i} style={{ display: 'flex', flexDirection: 'row', marginTop: 5, marginBottom: 5 }}>
              {m}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
