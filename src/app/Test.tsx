import * as React from 'react';
import parser, { INeuralItem, NeuralEmotionTypeEnum } from './lib/parser';
import ParserController from './lib/parser';
import { TensorFlowImplementation } from './lib/tensorflow';
const compatibility = require('./compatibility');
const bbfFace = require('./bbf_face');
const jsfeat = require('jsfeat');
jsfeat.bbf.face_cascade = bbfFace;

const DEFAULT_WIDTH = 640;
const DEFAULT_HEIGHT = 480;

interface IWorkAreaData {
  canvasWidth: number;
  canvasHeight: number;
  canvas2DContext: CanvasRenderingContext2D;
  workCanvasWidth: number;
  workCanvasHeight: number;
  workMatrixT: any;
  workCanvas: HTMLCanvasElement;
  workCanvas2DContext: CanvasRenderingContext2D;
}

interface IAppState {
  src: string;
  video: HTMLVideoElement;
  videoInited: boolean;
  canvas: HTMLCanvasElement;
  canvasInited: boolean;
  workAreaData: IWorkAreaData;
  error: string;

  parser: ParserController;
  neuralController: TensorFlowImplementation;
  dataPredict: { key: string; value: number }[];
}

const maxWorkSize = 100;

export default class App extends React.Component<any, IAppState> {
  constructor(props) {
    super(props);
    this.state = {
      src: null,
      video: null,
      videoInited: false,
      canvas: null,
      canvasInited: false,
      workAreaData: null,
      error: null,
      parser: null,
      neuralController: null,
      dataPredict: null
    };
  }

  componentDidMount() {
    let { error, parser, neuralController } = this.state;
    parser = new ParserController();
    let cachedPartitials = parser.getCachedPartitials();
    if (cachedPartitials.length <= 0)
      error = 'Не найдена обученная модель в Вашем локальном хранилище в браузере. Обучите модель, прежде чем ей пользоваться (раздел Train)';
    else {
      neuralController = new TensorFlowImplementation(parser, (...msg) => console.log(...msg));
    }
    this.setState({ error, parser, neuralController });
  }

  onPridict = (data: number[]) => {
    if (this.state.neuralController) {
      return this.state.neuralController.predict(data);
    }
    return null;
  };

  onInit = () => {
    let { video, videoInited, canvas, canvasInited, error, src } = this.state;
    if (videoInited && canvasInited) {
      if (!video || !canvas) return this.setState({ error: 'Canvas or video undefined' });
      try {
        compatibility.getUserMedia(
          { video: true },
          async stream => {
            try {
              src = compatibility.URL.createObjectURL(stream);
            } catch (error) {
              src = stream;
            } finally {
              await new Promise(resolve =>
                this.setState(
                  {
                    src
                  },
                  () => resolve()
                )
              );
              setTimeout(() => {
                video.play();
              }, 500);
            }
          },
          error => {
            error = 'Not available';
          }
        );
      } catch (error) {
        error = 'Some error';
      }
      this.setState({
        error
      });
    }
  };

  onDimensionsReady = async (videoWidth: number, videoHeight: number) => {
    let workAreaData = this.onInitWorkArea(videoWidth, videoHeight);
    await new Promise(resolve => this.setState({ workAreaData }, () => resolve()));
    compatibility.requestAnimationFrame(this.onFrame);
  };

  onInitWorkArea = (videoWidth: number, videoHeight: number) => {
    let { canvas } = this.state;
    let canvasWidth = canvas.width;
    let canvasHeight = canvas.height;
    let canvas2DContext = canvas.getContext('2d');

    canvas2DContext.fillStyle = 'rgb(0,255,0)';
    canvas2DContext.strokeStyle = 'rgb(0,255,0)';

    let workCanvasScaleFromVideo = Math.min(maxWorkSize / videoWidth, maxWorkSize / videoHeight);
    let workCanvasWidth = (videoWidth * workCanvasScaleFromVideo) | 0;
    let workCanvasHeight = (videoHeight * workCanvasScaleFromVideo) | 0;

    let workMatrixT = new jsfeat.matrix_t(workCanvasWidth, workCanvasHeight, jsfeat.U8_t | jsfeat.C1_t);
    let workCanvas = document.createElement('canvas') as HTMLCanvasElement;
    workCanvas.width = workCanvasWidth;
    workCanvas.height = workCanvasHeight;
    let workCanvas2DContext = workCanvas.getContext('2d');

    jsfeat.bbf.prepare_cascade(jsfeat.bbf.face_cascade);
    return {
      canvasWidth,
      canvasHeight,
      canvas2DContext,
      workCanvasWidth,
      workCanvasHeight,
      workMatrixT,
      workCanvas,
      workCanvas2DContext
    } as IWorkAreaData;
  };

  drawFaces = (rectangles: Array<any>, scale: number) => {
    let { canvas2DContext } = this.state.workAreaData;
    let rectanglesCount = Math.min(rectangles.length, 1);
    if (rectanglesCount) {
      jsfeat.math.qsort(rectangles, 0, rectanglesCount - 1, function(a, b) {
        return b.confidence < a.confidence;
      });
    }
    for (var i = 0; i < rectanglesCount; ++i) {
      let rectangle = rectangles[i];
      let rectangleX = (rectangle.x * scale) | 0;
      let rectangleY = (rectangle.y * scale) | 0;
      let rectangleWidth = (rectangle.width * scale) | 0;
      let rectangleHeight = (rectangle.height * scale) | 0;

      canvas2DContext.strokeRect(rectangleX, rectangleY, rectangleWidth, rectangleHeight);
      let image = canvas2DContext.getImageData(rectangleX, rectangleY, rectangleWidth, rectangleHeight);

      let res = this.onPridict(image.data as any);
      if (res) {
        this.setState({ dataPredict: res });
      }
    }
  };

  onFrame = () => {
    let { workAreaData, video } = this.state;
    let { canvasWidth, canvasHeight, workCanvas, workCanvas2DContext, workCanvasHeight, workCanvasWidth, canvas2DContext, workMatrixT } = workAreaData;
    compatibility.requestAnimationFrame(this.onFrame);
    if (video.readyState === video.HAVE_ENOUGH_DATA) {
      canvas2DContext.drawImage(video, 0, 0, canvasWidth, canvasHeight);
      workCanvas2DContext.drawImage(video, 0, 0, workCanvasWidth, workCanvasHeight);

      let image = workCanvas2DContext.getImageData(0, 0, workCanvasWidth, workCanvasHeight);
      jsfeat.imgproc.grayscale(image.data, workCanvasWidth, workCanvasHeight, workMatrixT);

      let pyramidBBF = jsfeat.bbf.build_pyramid(workMatrixT, 24 * 2, 24 * 2, 4);
      let rectangles = jsfeat.bbf.detect(pyramidBBF, jsfeat.bbf.face_cascade);

      let groupRectangles = jsfeat.bbf.group_rectangles(rectangles, 1);
      this.drawFaces(groupRectangles, canvasWidth / workMatrixT.cols);
    }
  };

  onLoadedData = (attempts?: number) => {
    let { video } = this.state;
    attempts = attempts || 0;
    if (video.videoWidth > 0 && video.videoHeight > 0) {
      this.onDimensionsReady(video.videoWidth, video.videoHeight);
    } else {
      if (attempts < 10) {
        attempts++;
        setTimeout(() => this.onLoadedData(attempts), 200);
      } else {
        this.onDimensionsReady(DEFAULT_WIDTH, DEFAULT_HEIGHT);
      }
    }
  };

  onLoadVideo = async (e: HTMLVideoElement) => {
    let { videoInited, video } = this.state;
    if (videoInited) return;
    videoInited = true;
    video = e;
    await new Promise(resolve => this.setState({ video, videoInited }, () => resolve()));
    this.onInit();
  };

  onLoadCanvas = async (e: HTMLCanvasElement) => {
    let { canvasInited, videoInited, canvas } = this.state;
    if (canvasInited) return;
    canvasInited = true;
    canvas = e;
    await new Promise(resolve => this.setState({ canvas, canvasInited }, () => resolve()));
    this.onInit();
  };

  render() {
    let { error, src } = this.state;
    return (
      <div id="App">
        <video
          ref={e => this.onLoadVideo(e)}
          width={DEFAULT_WIDTH}
          height={DEFAULT_HEIGHT}
          style={{ display: 'none' }}
          src={src}
          onLoadedData={() => this.onLoadedData()}
        />
        <canvas ref={e => this.onLoadCanvas(e)} width={DEFAULT_WIDTH} height={DEFAULT_HEIGHT} />
        {error && <div>{error}</div>}
        {this.state.dataPredict &&
          this.state.dataPredict.map(m => (
            <div>
              {m.key} - {m.value}
            </div>
          ))}
      </div>
    );
  }
}
