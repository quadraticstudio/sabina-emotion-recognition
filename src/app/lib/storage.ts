class StorageAbstract {
  get(key: string) {
    let val = localStorage.getItem(key);
    return val != null ? JSON.parse(val) : null;
  }

  set<T>(key: string, value: T) {
    let val = value != null ? JSON.stringify(value) : null;
    localStorage.setItem(key, val);
  }
}

export class StorageController extends StorageAbstract {
  get trained() {
    return this.get('trained');
  }

  set trained(value: number) {
    this.set('trained', value);
  }
}
