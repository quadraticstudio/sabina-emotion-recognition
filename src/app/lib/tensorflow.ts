import * as tf from '@tensorflow/tfjs';
import ParserController, { INeuralItem, NeuralUsageTypeEnum, NeuralEmotionTypeEnum, ALL_DATA_SET, NUM_CLASSES } from './parser';
import { toCategorical } from './helper';
import { StorageController } from './storage';

const epochs = 30;
const BATCH_SIZE_SCALE = 10;

export class TensorFlowImplementation {
  parserController: ParserController = null;
  storageController: StorageController = new StorageController();
  currentDataIndex: number = 0;

  items: INeuralItem[] = [];

  private model: tf.Sequential;
  private log: (...msg) => any;

  constructor(parserController: ParserController, log: (...msg) => any) {
    this.parserController = parserController;
    this.log = log;
  }

  async start() {
    await this.init();
    let trained = this.storageController.trained + 1;
    for (let trained = this.storageController.trained + 1; trained < ALL_DATA_SET; trained++) {
      await this.log(`----- Prepare to train by index ${trained} -----`);
      await this.startPartitial(trained);
      this.storageController.trained = trained;
    }
  }

  async startPartitial(index: number) {
    if (index >= ALL_DATA_SET || !this.parserController.existsIndex(index)) return null;
    let partitial = this.parserController.getPartitial(index);
    if (!partitial) return null;
    await partitial.load();
    let batchSize = Math.floor(partitial.train.length / BATCH_SIZE_SCALE);
    for (let offset = 0; offset < partitial.train.length; offset += batchSize) {
      let batch = partitial.nextTrainBatch(batchSize, offset);
      let xTrainTensor = batch.batchPixelsTensor.reshape([batch.batchPixelsTensor.shape[0], 48, 48, 1]);
      xTrainTensor = xTrainTensor.asType('float32');
      await this.log(`${xTrainTensor.shape[0]} train samples`);
      await this.train(xTrainTensor, batch.batchEmotionsTensor, batchSize);

      batch.batchPixelsTensor.dispose();
      batch.batchEmotionsTensor.dispose();
      xTrainTensor.dispose();
    }
    await this.saveModel();
    batchSize = Math.floor(partitial.test.length / BATCH_SIZE_SCALE);
    for (let offset = 0; offset < partitial.test.length; offset += batchSize) {
      let batch = partitial.nextTestBatch(batchSize, offset);
      let xTestTensor = batch.batchPixelsTensor.reshape([batch.batchPixelsTensor.shape[0], 48, 48, 1]);
      xTestTensor = xTestTensor.asType('float32');
      await this.log(`${xTestTensor.shape[0]} test samples`);
      await this.test(xTestTensor, batch.batchEmotionsTensor);

      batch.batchPixelsTensor.dispose();
      batch.batchEmotionsTensor.dispose();
      xTestTensor.dispose();
    }
    await this.saveModel();
  }

  async test(xTensor: tf.Tensor, yTensor: tf.Tensor) {
    let score = this.model.evaluate(xTensor, yTensor);
    await this.log(`Test loss: ${score[0]}`);
    await this.log(`Test accuracy: ${score[1]}`);
  }

  async train(xTensor: tf.Tensor, yTensor: tf.Tensor, batchSize: number) {
    let res = await this.model.fit(xTensor, yTensor, { batchSize, stepsPerEpoch: 256, epochs: epochs });
    const loss = res.history.loss[0];
    const accuracy = res.history.acc[0];
    await this.log(`Train loss: ${loss}`);
    await this.log(`Train accuracy: ${accuracy}`);
  }

  predict(data: number[]) {
    data = data.map(m => m / 255);
    let xPredictTensor = tf.tensor2d([data], [1, data.length], 'float32');

    xPredictTensor = xPredictTensor.reshape([xPredictTensor.shape[0], 48, 48, 1]) as any;
    xPredictTensor = xPredictTensor.asType('float32');

    let res = this.model.predict(xPredictTensor);
    let dataResult = res instanceof Array ? res[0] : res;
    let rawResult = dataResult.toString();
    return this.getAllResults(rawResult)
      .map(m => Number.parseFloat(m))
      .map((m, i) => ({ key: NeuralEmotionTypeEnum[i], value: m }));
  }

  private getAllResults(rawString: string) {
    const regex = /[\d.]+/gm;
    let m: RegExpExecArray;
    let results: string[] = [];
    while ((m = regex.exec(rawString)) !== null) {
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      results.push(...m);
    }
    return results;
  }

  private async init() {
    if (!this.model) {
      let model = await this.loadModel();
      if (this.storageController.trained < 0) {
        model.add(tf.layers.conv2d({ filters: 64, kernelSize: [5, 5], activation: 'relu', inputShape: [48, 48, 1] }));
        model.add(tf.layers.maxPooling2d({ poolSize: [5, 5], strides: [2, 2] }));

        model.add(tf.layers.conv2d({ filters: 64, kernelSize: [3, 3], activation: 'relu' }));
        model.add(tf.layers.conv2d({ filters: 64, kernelSize: [3, 3], activation: 'relu' }));
        model.add(tf.layers.averagePooling2d({ poolSize: [3, 3], strides: [2, 2] }));

        model.add(tf.layers.conv2d({ filters: 128, kernelSize: [3, 3], activation: 'relu' }));
        model.add(tf.layers.conv2d({ filters: 128, kernelSize: [3, 3], activation: 'relu' }));
        model.add(tf.layers.averagePooling2d({ poolSize: [3, 3], strides: [2, 2] }));

        model.add(tf.layers.flatten());

        model.add(tf.layers.dense({ units: 1024, activation: 'relu' }));
        model.add(tf.layers.dropout({ rate: 0.2 }));
        model.add(tf.layers.dense({ units: 1024, activation: 'relu' }));
        model.add(tf.layers.dropout({ rate: 0.2 }));

        model.add(tf.layers.dense({ units: NUM_CLASSES, activation: 'softmax' }));
        model.compile({
          optimizer: tf.train.adam(),
          loss: 'categoricalCrossentropy',
          metrics: ['accuracy']
        });
        this.model = model;
      }
    }
  }

  private async loadModel() {
    let model: tf.Sequential = null;
    try {
      model = (await tf.loadModel('indexeddb://sabina-model')) as any;
      if (!model) throw new Error();
    } catch (err) {
      await this.log('not found model in indexeddb');
      model = tf.sequential();
      this.storageController.trained = -1;
    } finally {
      this.model = model;
      return model;
    }
  }

  private async saveModel() {
    try {
      let res = await this.model.save('indexeddb://sabina-model');
      await this.log('model successfully wrote');
    } catch (err) {
      await this.log(err);
      await this.log('model with error wrote');
    }
  }
}
